#!/bin/bash

# This line is needed for running the cricket cicada detector with Apptainer. (When converting the Docker container to
# Apptainer, the working directory WORKDIR is not handled correctly. This is fixed by explicitly changing the directory
# here.)
cd /usr/src/app/src || exit

python3 arise_server.py "$@";

# After analyzing the current batch, remove the audio files from the input directory to prepare for the next batch.
rm /usr/src/app/algorithm_input/*.*
