import json
import requests
import unittest

from schemas import validate_dsi_predictions


class TestApi(unittest.TestCase):
    def test_api_single(self):
        # Here we invoke the algorithm, using the media file we provided
        response = requests.post(
            "http://localhost:5000/v1/analyse",
            files={"media": open("algorithm_input/XC911393--Field-Cricket--Gryllus-campestris.wav", "rb")},
        )
        self.assertEqual(200, response.status_code)

        # Here we use the official validation script
        json_response = response.json()
        print(json.dumps(json_response, indent=2))
        json.dump(json_response, open("api_response.json", "w"), indent=2)
        validator_result = validate_dsi_predictions.validate_json_predictions_file("api_response.json")
        self.assertEqual(0, validator_result)

        # Here we can add a couple of checks of our own, if desired
        # These extra checks will depend on any extra logic you have in your algorithm output
        prediction_count = 1
        self.assertEqual(1, len(json_response["media"]))
        self.assertEqual(prediction_count, len(json_response["region_groups"]))
        self.assertEqual(prediction_count, len(json_response["predictions"]))

        print(
            f"Algorithm output covers {len(json_response['media'])} media file, "
            f"{len(json_response['region_groups'])} region group, "
            f"and {len(json_response['predictions'])} prediction set."
        )
