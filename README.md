This repository contains the cricket and cicada detector algorithm created by Capgemini.

To build the Docker container:

    docker build . -t cricket-cicada-detector

To build the Docker container image for the container registry and push the image to the registry:

    docker build -t registry.gitlab.com/arise-biodiversity/dsi/algorithms/cricket-cicada-detector-capgemini:1.1.2 .
    docker push registry.gitlab.com/arise-biodiversity/dsi/algorithms/cricket-cicada-detector-capgemini:1.1.2

To start the container:

    docker run -p 5000:5000 cricket-cicada-detector

To get ready for testing your algorithm's output, install the environment:

    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.test.txt
    git clone "https://gitlab.com/arise-biodiversity/schemas.git" tests/schemas

To test your API, including JSON schema checking, make sure the Docker container has been started (as above) and then run:

    source venv/bin/activate
    pytest --capture=no

## Running with memory and cpu limits

docker run -m 4g --cpus=1 -v $PWD/algorithm_input:/usr/src/app/algorithm_input:z -v $PWD/algorithm_output:/usr/src/app/algorithm_output:z -it cricket-cicada-detector


## Run confusion matrix calculation

Download data from https://zenodo.org/records/8252141, "InsectSet66_Train_Val_Test.zip" and "InsectSet66_Train_Val_Test_Annotation.csv".
Extract the zip contents to `algorithm_input`, and the annotations to the root of the project, then run:

```sh
python3 confusion_matrix.py
```