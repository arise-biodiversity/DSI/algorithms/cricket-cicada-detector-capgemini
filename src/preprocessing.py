import torch

import numpy as np


def loop_short(audio, sample_buffer, samples_total):
        """
        Loop short audio files until the sample_buffer length is reached.

        Args:
            audio: array, audio waveform.
            sample_buffer: float, total sample buffer length.
                           Calculated as window_size*sample_rate.
            samples_total: int, total number of samples for calculating the amount of loops.

        Returns:
              looped audiofile

        """

        count = int(sample_buffer / samples_total) + (sample_buffer % samples_total > 0)
        i = 1                                                              
        loop = audio                                                    

        while i < count:
            loop = np.concatenate([loop, audio])             
            i += 1                                                    

        loop = loop[: int(sample_buffer)]
        # create a tensor from np.array and change shape of the tensor using view
        return torch.from_numpy(loop).view(1, -1)

        
def pad_short(audio, sample_buffer, samples_total):
        """
        Pad short audio files until the sample_buffer length is reached.

        Args:
            audio: array, audio waveform.
            sample_buffer: float, total sample buffer length.
                           Calculated as window_size*sample_rate.
            samples_total: int, total number of samples for calculating the amount of loops.

        Returns:
             padded audiofile

        """

        pad = int(sample_buffer - samples_total)
        wave = np.pad(audio, (0, pad))
        # create a tensor from np.array and change shape of the tensor using view
        return torch.from_numpy(wave).view(1, -1)


def chunk_long(audio, sample_rate, samples_total):
    """
    Chunk audio files into small overlapping fixed size windows.

    Args:
        audio: array, audio waveform.
        sample_rate: int, sample rate of audio files.
        samples_total: int, total number of samples for calculating the amount of loops.

    Returns:
        list of audio chunks.
    """

    # 5.5 sec chunks per sample
    chunk_size = int(5.5 * sample_rate)
    # 2.75 since this chunk size was used to optimize the model
    overlap_samples = 2.75*sample_rate

    chunks = []
    chunk_start = 0

    while chunk_start < samples_total:
        chunk_end = min(samples_total, chunk_start + chunk_size)
        chunk = audio[chunk_start:chunk_end].clone()
        chunk = chunk.unsqueeze(0)
        chunks.append(chunk)

        chunk_start = int(chunk_start + chunk_size - overlap_samples)

    return chunks
