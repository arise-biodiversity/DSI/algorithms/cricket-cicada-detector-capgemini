import json


def create_and_write_ontology(classes_names: list[str], output_json_file_path: str):
    print(f"Create an ontology with {len(classes_names)} class names and write it to file {output_json_file_path}.")

    name_ontology_json = {
        "name": "cricket-cicada-detector"
    }

    ontology_items_json = {}

    for class_name in classes_names:
        ontology_items_json[class_name] = {}

    name_ontology_json["items"] = ontology_items_json

    with open(file=output_json_file_path, mode="w") as output_json_file:
        json.dump(obj=name_ontology_json, fp=output_json_file, indent=4)
