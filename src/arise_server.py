import argparse
import datetime
import flask
import json
import numpy as np
import os
import tempfile
import torch
import torchaudio

from collections import namedtuple
from pathlib import Path
from torch import Tensor
from torch.nn.functional import softmax
from typing import Any, Union
from werkzeug.datastructures import FileStorage

from preprocessing import chunk_long, loop_short, pad_short


app = flask.Flask(__name__, static_url_path="", static_folder="static")


Prediction = namedtuple(typename="Prediction", field_names=["scientific_name", "scientific_name_id", "probability", "t1", "t2"])


class CricketCicadaDetector:
    def __init__(self):
        self._algorithm_mode = self._determine_algorithm_mode()

        print()
        print(f"Running the cricket cicada detector in {self._algorithm_mode} mode.")
        print()

        # Set working directory - parent is needed since filepath would include the Python filename.
        working_directory = Path(__file__).resolve().parent
        print(f"Change the directory to '{working_directory}'.")
        os.chdir(working_directory)

        # Load fully trained model in Torch script format.
        self._loaded_model = torch.jit.load('model_traced.pt')

        # Create the ontology and write it to the JSON file.
        # from create_ontology import create_and_write_ontology
        # create_and_write_ontology(self.classes_names, output_json_file_path="../cricket-cicada-detector_name_ontology.json")

    def run_algorithm(self):
        if self._algorithm_mode == "directories":
            self.analyse_directories("../algorithm_input", "../algorithm_output")
        else:
            app.run(
                debug=self._str2bool(os.getenv("DEBUG")),
                host=os.getenv("HOST", "0.0.0.0"),
                port=int(os.getenv("PORT", "5000")),
                use_reloader=False,
            )

    def analyse_directories(self, input_directory: str, output_directory: str) -> None:
        print(f"Reading media files from the '{input_directory}' directory.")

        # Get the media file paths.
        input_paths = [
            os.path.join(input_directory, file_name)
            for file_name in os.listdir(input_directory)
            if os.path.isfile(os.path.join(input_directory, file_name))
        ]

        # Analyse the media files.
        analysis_results, _ = self.analyse(input_paths)

        # Write the analysis results to a JSON file in the output directory.
        results_path = os.path.join(output_directory, "analysis-results.json")
        message = f"Write the analysis results to the JSON file '{results_path}'."
        print()
        print(message)
        with open(results_path, "w", encoding="utf-8") as results_file:
            json.dump(analysis_results, results_file, ensure_ascii=False, indent=4)  # type: ignore

    def analyse(self, input_files: list[Union[str, FileStorage]]) -> tuple[dict[str, Any], int]:
        """
        Analyses a batch of audio files.

        :param input_files: Either a list of filepaths to audio files (directories mode) or a list of Flask/Werkzeug
                            FileStorage objects (endpoint mode).
        """
        if input_files:
            print(f"Number of input files: {len(input_files)}.")
        else:
            print("Error: no input files found.")
            return {"error": "No input files found."}, 400

        analysis_results = []

        # For each audio file perform inference.
        for input_file in input_files:
            file_name = os.path.basename(input_file) if self._algorithm_mode == "directories" else input_file.filename

            if file_name != ".gitignore":
                print()
                print(f"Processing audio file '{file_name}'.")

                # Validate the audio file format.
                allowed_audio_extensions = {".wav", ".mp3"}
                _, file_extension = os.path.splitext(file_name)
                if file_extension.lower() not in allowed_audio_extensions:
                    print(f"Invalid audio file format: {file_name}.")
                    return {"error": "Invalid audio input, expected files: .wav, .mp3."}, 415

                with tempfile.TemporaryDirectory() as tmp_dir:
                    # Write the audio file to a temporary directory.
                    audio_file_path = os.path.join(tmp_dir, file_name)
                    with open(audio_file_path, "wb") as output_file:
                        if self._algorithm_mode == "directories":
                            with open(input_file, "rb") as input_file_stream:
                                output_file.write(input_file_stream.read())
                        else:
                            output_file.write(input_file.read())

                    # Print class names and probabilities for debugging.
                    # audio_length, max_probability, predicted_class, probabilities = self.predict_species(audio_file_path)
                    audio_length, max_probability, predicted_class, _ = self.predict_species(audio_file_path)

                    # Print class names and probabilities for debugging.
                    # self._print_classes_and_probabilities(file_name, probabilities)

                    # Add the prediction with the highest probability for this audio file.
                    prediction = Prediction(
                        scientific_name=self.classes_names[predicted_class.item()],
                        scientific_name_id=f"LITERAL:CLASS {predicted_class.item()}",
                        probability=np.round(max_probability.item(), 6),
                        t1=0.0,
                        t2=audio_length,
                    )

                    analysis_results.append((file_name, prediction))

        return self.format_analysis_results(analysis_results), 200

    def predict_species(self, audio_file_path: str) -> tuple[float, Tensor, Tensor, Tensor]:
        # load waveform from file path
        wav, sample_rate = torchaudio.load(audio_file_path)

        # Preprocessing
        predictions = []
        samples_total = len(wav[0])

        # 5.5 sec chunk size for each sample
        sample_buffer = 5.5 * sample_rate

        # if audiofile is smaller than 5.5 sec
        if samples_total < sample_buffer:
            predictions.append(self._loaded_model(pad_short(wav[0], sample_buffer, samples_total)))
            predictions.append(self._loaded_model(loop_short(wav[0], sample_buffer, samples_total)))
        # else audiofile is larger than 5.5 sec
        else:
            chunks = chunk_long(wav[0], sample_rate, samples_total)
            # fit each chunk into the model
            for chunk in chunks:
                predictions.append(self._loaded_model(chunk))

        # perform model inference
        predictions = torch.cat(predictions)
        predictions = predictions.detach().numpy()

        # get component wise mean prediction for each chunk
        average_predictions = np.mean(predictions, axis=0)

        # transfer raw output (logits) to probabilities
        probabilities = softmax(torch.from_numpy(average_predictions).view(1, -1), dim=1)
        max_probability, predicted_class = torch.max(probabilities, dim=1)

        return samples_total / sample_rate, max_probability, predicted_class, probabilities

    def format_analysis_results(self, analysis_results: list[tuple[str, Prediction]]):
        """ Format the analysis results into the ARISE DSI JSON format. """
        analysis_results_json = {
            "name": "https://schemas.arise-biodiversity.nl/dsi/multi-object-multi-image#sequence-one-prediction-per-region",
            # Note: the schema name that is used above is not an actual working URL, but it has been used by convention.
            #       If you want to view the contents of the corresponding schema, you can use this URL (which points to
            #       the public ARISE schemas repository):
            #       https://gitlab.com/arise-biodiversity/schemas/-/raw/main/schema_dsi_predictions.json
            "generated_by": {
                "datetime": datetime.datetime.now().isoformat() + "Z",
                "version": f"cricket cicada detector version {os.getenv('ALGORITHM_VERSION', '1.1.2')}",
                "tag": os.getenv("ALGORITHM_TAG", "algorithm tag unknown"),
            },
            "media": [],
            "region_groups": [],
            "predictions": [],
        }

        for file_name, prediction in analysis_results:
            media_id = os.path.splitext(os.path.split(file_name)[-1])[0]

            analysis_results_json["media"].append(
                {
                    "filename": file_name,
                    "id": media_id,
                }
            )

            region_group_id = f"{media_id}?region=0"

            analysis_results_json["region_groups"].append(
                {
                    "id": region_group_id,
                    # Because individuals are not tracked (yet), we re-use the region_group_id as individual_id.
                    "individual_id": region_group_id,
                    "regions": [
                        {
                            "media_id": media_id,
                            "box": {
                                "t1": prediction.t1,
                                "t2": prediction.t2,
                            }
                        }
                    ],
                }
            )

            analysis_results_json["predictions"].append(
                {
                    "region_group_id": region_group_id,
                    "taxa": {
                        "type": "singleclass",
                        "items": [
                            {
                                "scientific_name": prediction.scientific_name,
                                "scientific_name_id": prediction.scientific_name_id,
                                "probability": prediction.probability,
                            }
                        ],
                    },
                }
            )

        return analysis_results_json

    def _determine_algorithm_mode(self):
        """Determine the mode ("directories" or "endpoint") the algorithm will run in. First the "--algorithm-mode"
           command-line argument is used to determine the algorithm mode. If that is not specified, the "ALGORITHM_MODE"
           environment variable determines the mode. If both are not specified, algorithm mode "directories" is used by
           default."""
        argument_parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

        argument_parser.add_argument(
            "--algorithm-mode",
            type=str,
            default=os.getenv("ALGORITHM_MODE", "directories"),
            help="Whether the algorithm interface uses input/output directories or an endpoint.",
        )

        command_line_arguments = argument_parser.parse_args()

        return command_line_arguments.algorithm_mode

    def _str2bool(self, value) -> bool:
        """
        Converts a string to boolean by matching it with some well known representations of bool.
        {"yes", "true", "t", "y", "1"} --> true
        If value is not one of these representations it returns False.
        :param value: a string representing a boolean value
        :return: a boolean value
        """
        return str(value).lower() in {"yes", "true", "t", "y", "1"}

    # Print class names and probabilities for debugging.
    # def _print_classes_and_probabilities(self, file_name: str, probabilities: Tensor):
    #     print()
    #     print("Classes and probabilities:")
    #     # Note: the expected_class_name part only works for test input files with specific naming conventions.
    #     expected_class_name = file_name.split("--")[-1].split(".")[0].replace("-", "").replace(" ", "")
    #     classes_and_probabilities = sorted(zip(self.classes_names, probabilities.tolist()[0]), key=lambda pair: pair[1])
    #
    #     for class_index, class_and_probability in enumerate(classes_and_probabilities):
    #         class_name = class_and_probability[0]
    #         probability_percentage = 100 * class_and_probability[1]
    #         marker_expected = ">" if class_name == expected_class_name else "-"
    #         marker_actual = "+" if class_index == len(self.classes_names) - 1 else marker_expected
    #         marker = "*" if marker_actual == "+" and marker_expected == ">" else marker_actual
    #         if marker != "-":
    #             print(f"{len(self.classes_names) - class_index:02d}. {marker} {class_name}: {probability_percentage:0.1f}%.")

    # noinspection SpellCheckingInspection
    classes_names = sorted([
        'Aleetacurvicosta',
        'Atrapsaltacollina',
        'Atrapsaltacorticina',
        'Atrapsaltaencaustica',
        'Cicadaorni',
        'Clinopsaltaautumna',
        'Cyclochilaaustralasiae',
        'Diceroproctaeugraphica',
        'Galangalabeculata',
        'Neotibicenpruinosus',
        'Platypleuracfcatenata',
        'Platypleuraplumosa',
        'Platypleurasp10',
        'Platypleurasp12cfhirtipennis',
        'Platypleurasp13',
        'Popplepsaltaaeroides',
        'Popplepsaltanotialis',
        'Psaltodaplaga',
        'Yoyettacelis',
        'Yoyettarepetens',
        'Chorthippusalbomarginatus',
        'Chorthippusapricarius',
        'Chorthippusbiguttulus',
        'Chorthippusbrunneus',
        'Chorthippusmollis',
        'Chorthippusvagans',
        'Chrysochraondispar',
        'Gomphocerippusrufus',
        'Gomphocerussibiricus',
        'Myrmeleotettixmaculatus',
        'Omocestuspetraeus',
        'Omocestusrufipes',
        'Omocestusviridulus',
        'Pseudochorthippusmontanus',
        'Pseudochorthippusparallelus',
        'Stauroderusscalaris',
        'Stenobothruslineatus',
        'Stenobothrusstigmaticus',
        'Achetadomesticus',
        'Eumodicogryllusbordigalensis',
        'Gryllusbimaculatus',
        'Grylluscampestris',
        'Melanogryllusdesertus',
        'Oecanthuspellucens',
        'Barbitistesyersini',
        'Bicoloranabicolor',
        'Conocephalusdorsalis',
        'Conocephalusfuscus',
        'Decticusverrucivorus',
        'Ephippigerdiurnus',
        'Eupholidopteraschmidti',
        'Gampsocleisglabra',
        'Leptophyespunctatissima',
        'Metriopterabrachyptera',
        'Phaneropterafalcata',
        'Phaneropteranana',
        'Pholidopteraaptera',
        'Pholidopteragriseoaptera',
        'Pholidopteralittoralis',
        'Platycleisalbopunctata',
        'Roeselianaroeselii',
        'Ruspolianitidula',
        'Tettigoniacantans',
        'Tettigoniaviridissima',
        'Tylopsislilifolia',
        'Nemobiussylvestris'
    ])


cricket_cicada_detector = CricketCicadaDetector()


@app.route("/v1/analyse", methods=["POST"])
def analyse_endpoint():
    """
    Analyses a batch of audio files.

    Expects POST parameter 'media'.
    """
    uploaded_files = flask.request.files.getlist("media")

    analysis_results, status_code = cricket_cicada_detector.analyse(uploaded_files)

    if status_code == 200:
        return flask.jsonify(analysis_results)
    else:
        return analysis_results, status_code


if __name__ == "__main__":
    cricket_cicada_detector.run_algorithm()
