FROM python:3.11.7-slim-bookworm

LABEL org.opencontainers.image.authors="Team ARISE DSI: j.w.kamminga@utwente.nl"

RUN apt update && apt install -y --allow-unauthenticated git nano rsync screen && apt clean \
&& python -m pip install --no-cache-dir -U pip \
&& python -m pip install --no-cache-dir -U setuptools

COPY requirements.txt /tmp/
RUN pip install --requirement /tmp/requirements.txt

COPY . /usr/src/app/
WORKDIR /usr/src/app

ENV PYTHON_UNBUFFERED=1

# CMD ["python", "src/arise_server.py"]
ENTRYPOINT ["/usr/src/app/start.sh"]
