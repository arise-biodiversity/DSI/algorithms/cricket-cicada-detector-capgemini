from sklearn.metrics import confusion_matrix, f1_score
import pandas as pd
import numpy as np
import json

def main():
    file_path = 'algorithm_output/analysis-results.json'
    csv_file = 'InsectSet66_Train_Val_Test_Annotation.csv'

    data = pd.read_csv(csv_file)

    with open(file_path, 'r') as file:
        predictions_json = json.load(file)

    predictions = []

    for pred in predictions_json['predictions']:
        for item in pred['taxa']['items']:
            predictions.append({
                'region_group_id': pred['region_group_id'].split("?")[0],
                'predicted_species': item['scientific_name']
            })

    predictions_df = pd.DataFrame(predictions)
    print(predictions_df)

    # Assuming 'unique_file' in CSV can be mapped to 'region_group_id' in predictions
    result_df = pd.merge(data, predictions_df, left_on='unique_file', right_on='region_group_id')

    print(result_df)
    print("save merged dataframe")
    result_df.to_csv('merged_df.csv')

    # Compute confusion matrix
    y_true = result_df['species']
    y_pred = result_df['predicted_species']
    conf_matrix = confusion_matrix(y_true, y_pred, labels=list(set(y_true)))
    np.savetxt("confusion_matrix.csv", conf_matrix, fmt='%i', delimiter=",")


    print("conf_matrix")
    print(conf_matrix)


    f1_micro = f1_score(y_true, y_pred, average='micro')
    f1_macro = f1_score(y_true, y_pred, average='macro')
    f1_weighted = f1_score(y_true, y_pred, average='weighted')

    print("F1 Score (Micro):", f1_micro)
    print("F1 Score (Macro):", f1_macro)
    print("F1 Score (Weighted):", f1_weighted)

if __name__ == "__main__":
    main()